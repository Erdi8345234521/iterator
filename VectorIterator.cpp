// VectorIterator.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "vector"

using namespace std;

template<typename VectorType>
class VectorIterator
{
	vector<VectorType> OwnerContainer;
	int CurrentPosition;
	VectorType CurrentValue;

public:
	//Init iterator
	VectorIterator(vector<VectorType> Collection)
	{
		OwnerContainer = Collection;
	}

	//Return next element from container
	VectorType GetNext()
	{
		if(HasMore())
		{
			CurrentValue = OwnerContainer[CurrentPosition];
			CurrentPosition++;
		}

		return CurrentValue;
	}

	//Returns true if there are still elements that the iterator did not pass through
	bool HasMore()
	{
		return CurrentPosition < OwnerContainer.capacity();
	}
};

int main()
{
	using StringContainer = vector<string>;
	using IntContainer = vector<int>;
	
	//Declare Containers
	StringContainer VCS = {"Test1", "Test2", "Test3", "Test4", "Test5"};
	IntContainer VCI = { 0, 1, 2, 3, 4 };

	//Declare Iterators
	VectorIterator<string> StrIterator(VCS);
	VectorIterator<int> IntIterator(VCI);
	
	//Passing through all the elements
	while (StrIterator.HasMore())
	{
		cout << "String iterator element value: " << StrIterator.GetNext() << "\n";
	}
	
	while (IntIterator.HasMore())
	{
		cout << "String iterator element value: " << IntIterator.GetNext() << "\n";
	}
	
	
    std::cout << "End program\n";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
